#!/usr/bin/python
# -*- coding: utf-8 -*-
# Example on how to read the ADXL345 accelerometer.

import time
import csv
import sys
import os
import collections
from adxl345 import ADXL345


if not sys.version_info >= (3, 0):
    sys.stdout.write("Sorry, requires Python 3.x, not Python 2.x. Please execute with `python3 main.py`\n")
    sys.exit(1)

from queue import Queue
q = Queue()


# ========== Constant Values ==========
headers = ['time', 'x', 'y', 'z']
PRINT_INTERVAL = 5
OFFSET_ms = 0.091
# =====================================


if __name__ == '__main__':

    print('========== Starting ADXL345 ==========')
    adxl = ADXL345()

    adxl.power_on()
    device_id = adxl.get_device_id()
    if device_id == 0xE5:
        print('Device found, ID: {}'.format(hex(device_id)))
    else:
        print('Wrong device ID - {}, please check connectivity.'.format(hex(device_id)))
        sys.exit(1)

    print('Now, let us set sensitivity. The lower range gives more resolution for slow movements, '
          'the higher range is good for high speed tracking.')
    while True:
        g_range = input('Sensitivity(2, 4, 8, 16): ')
        try:
            g_range = int(g_range)
        except ValueError:
            print('Please input Integer value')
            continue
        if g_range not in [2, 4, 8, 16]:
            print('Please input correct value')
            continue
        break
    adxl.set_range(g_range)

    output_file_name = input('Output file name: ')

    while True:
        rate = input('Data rate in Hz(1024 ~ 3200Hz): ')
        try:
            rate = int(rate)
        except ValueError:
            print('Please input Integer value')
            continue
        if rate < 1024:
            print('Too small, should be greater than 1024')
            continue
        break
    adxl.set_data_rate(rate)

    print('Would you like to calibrate now? If yes, please put the device on a flat surface and enter `y`')
    if input('Calibration: ') == 'y':
        print('Calibration offsets: {}'.format(adxl.calibrate()))

    while True:
        trigger = input('Enter trigger value(`n` to disable): ')
        if trigger == 'n':
            trigger_val = None
        else:
            try:
                trigger_val = float(trigger)
            except ValueError:
                print('Please input numeric value')
                continue
        break

    while True:
        preload_time = input('Enter pre-load time(ms):')
        try:
            preload_time = int(preload_time)
        except ValueError:
            print('Please input numeric value')
            continue
        break

    while True:
        file_save_time = input('Save time in seconds(Maximum 30 sec):')
        try:
            file_save_time = float(file_save_time)
        except ValueError:
            print('Please input numeric value')
            continue
        if not 0 < file_save_time < 30:
            print('Invalid value')
            continue
        break

    input('Hit ENTER to start...')

    interval = 1 / rate

    trigger_started = True if trigger_val is None else False
    trigger_start_time = -1

    file_index = 0

    printed_time = time.time()

    shift_reg = collections.deque([], int(rate * preload_time / 1000))

    try:
        while True:
            s_time = time.time()
            axis_data = adxl.get_axes()
            axis_data['time'] = s_time * 1000

            if time.time() - printed_time > PRINT_INTERVAL:
                print(axis_data, '     Elapsed(ms): ', (time.time() - s_time) * 1000)
                printed_time = time.time()

            if trigger_started:
                if time.time() - trigger_start_time < file_save_time:
                    q.put(axis_data)
                else:
                    file_name = '{}_{}.csv'.format(output_file_name, file_index)
                    print('Saving sensor data to the file - {}'.format(file_name))
                    with open(file_name, 'a') as out_csv:
                        writer = csv.DictWriter(out_csv, fieldnames=headers)
                        writer.writeheader()
                        # Save pre-loaded data
                        count = 0
                        while True:
                            try:
                                data = shift_reg.popleft()
                                writer.writerow(data)
                                count += 1
                            except IndexError:
                                break
                        print('Saved {} pre-loaded data'.format(count))
                        while not q.empty():
                            data = q.get()
                            writer.writerow(data)
                    file_index += 1
                    # Reset timer and flag
                    trigger_started = True if trigger_val is None else False
                    trigger_start_time = -1
            else:
                shift_reg.append(axis_data)
                if trigger_start_time < 0:
                    if any([abs(axis_data[_key]) > trigger_val for _key in ['x', 'y', 'z']]):
                        print('!!! {}  Current value - {} is larger than the threshold({}), saving...'.format(
                            time.time(), axis_data, trigger_val
                        ))
                        trigger_start_time = time.time()
                        trigger_started = True

            elapsed = time.time() - s_time
            # print("Elapsed(ms): {}, interval(ms): {}".format(elapsed * 1000, interval * 1000))
            time.sleep(max(0, interval - elapsed - OFFSET_ms / 1000))
    except KeyboardInterrupt:
        adxl.close()
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
