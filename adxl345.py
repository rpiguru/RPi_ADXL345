"""
    PYTHON base driver for the ADXL-345 (3 axes accelerometer from Analog Device)
    This class holds all methods that you should be able to call on client side.

"""

from __future__ import division
import math
import spidev


WRITE_MASK = 0x0
READ_MASK = 0x80
MULTIREAD_MASK = 0x40


class ADXL345:
    # Registers
    REG_DEVICE_ID = 0x00
    REG_THRESH_TAP = 0x1D
    REG_OFSX = 0x1E
    REG_OFSY = 0x1F
    REG_OFSZ = 0x20
    REG_DUR = 0x21
    REG_LATENT = 0x22
    REG_WINDOW = 0x23
    REG_THRESH_ACT = 0x24
    REG_THRESH_INACT = 0x25
    REG_TIME_INACT = 0x26
    REG_ACT_INACT_CTL = 0x27
    REG_THRESH_FF = 0x28
    REG_TIME_FF = 0x29
    REG_TAP_AXES = 0x2A
    REG_ACT_TAP_STATUS = 0x2B
    REG_BW_RATE = 0x2C
    REG_POWER_CTL = 0x2D
    REG_INT_ENABLE = 0x2E
    REG_INT_MAP = 0x2F
    REG_INT_SOURCE = 0x30
    REG_DATA_FORMAT = 0x31
    REG_DATAX0 = 0x32
    REG_DATAX1 = 0x33
    REG_DATAY0 = 0x34
    REG_DATAY1 = 0x35
    REG_DATAZ0 = 0x36
    REG_DATAZ1 = 0x37
    REG_FIFO_CTL = 0x38
    REG_FIFO_STATUS = 0x39

    # Full Resolution scale factor (0x100 LSB/g ~= 3.9/1000 mg/LSB)
    SCALE_FACTOR = 1 / 0x100

    def __init__(self, spi_bus=0, spi_device=0):
        self._full_resolution = True
        self._range = 0
        self.spi = spidev.SpiDev()
        self.spi.open(spi_bus, spi_device)
        self.spi.mode = 0b11
        self.spi.max_speed_hz = 1953000

    def get_register(self, address):
        value = self.spi.xfer2([(address & 0x3F) | READ_MASK, 0])
        return value[1]

    def get_registers(self, address, count):
        cmd = [(address & 0x3F) | READ_MASK | MULTIREAD_MASK] + [0] * count
        value = self.spi.xfer2(cmd)
        return value[1:]

    def set_register(self, address, value):
        self.spi.xfer2([address, value])

    def get_device_id(self):
        return self.get_register(ADXL345.REG_DEVICE_ID)

    def set_data_rate(self, hz, low_power=False):
        if hz >= 3200:
            rate = 3200
            rate_code = 0b1111
        elif 1600 <= hz < 3200:
            rate = 1600
            rate_code = 0b1110
        elif 800 <= hz < 1600:
            rate = 800
            rate_code = 0b1101
        elif 400 <= hz < 800:
            rate = 400
            rate_code = 0b1100
        elif 200 <= hz < 400:
            rate = 200
            rate_code = 0b1011
        elif 100 <= hz < 200:
            rate = 100
            rate_code = 0b1010
        elif 50 <= hz < 100:
            rate = 50
            rate_code = 0b1001
        elif 25 <= hz < 50:
            rate = 25
            rate_code = 0b1000
        elif 25 / 2 <= hz < 25:
            rate = 25 / 2
            rate_code = 0b0111
        elif 25 / 4 <= hz < 25 / 2:
            rate = 25 / 4
            rate_code = 0b0110
        elif 25 / 8 <= hz < 25 / 4:
            rate = 25 / 8
            rate_code = 0b0101
        elif 25 / 16 <= hz < 25 / 8:
            rate = 25 / 16
            rate_code = 0b0100
        elif 25 / 32 <= hz < 25 / 16:
            rate = 25 / 32
            rate_code = 0b0011
        elif 25 / 64 <= hz < 25 / 32:
            rate = 25 / 64
            rate_code = 0b0010
        elif 25 / 128 <= hz < 25 / 64:
            rate = 25 / 128
            rate_code = 0b0001
        elif hz < 25 / 128:
            rate = 25 / 256
            rate_code = 0
        else:
            print('Wrong frequency')
            return

        if low_power:
            rate_code = rate_code | 0x10

        self.set_register(ADXL345.REG_BW_RATE, rate_code)
        return rate

    @staticmethod
    def _equal(value, reference, error_margin=0.1):
        return (reference - error_margin) <= value <= (reference + error_margin)

    def _convert(self, lsb, msb, unit='g'):
        """ Convert the gravity data returned by the ADXL to meaningful values """
        value = lsb | (msb << 8)
        if value & 0x8000:
            value = -value ^ 0xFFFF
        if not self._full_resolution:
            value = value << self._range
        value *= ADXL345.SCALE_FACTOR
        if unit != 'g':
            # 1g = 9810 mm/s^2
            value *= 9810
        return value

    def _set_power_ctl(self, measure, wake_up=0, sleep=0, auto_sleep=0, link=0):
        power_ctl = wake_up & 0x03

        if sleep:
            power_ctl |= 0x04
        if measure:
            power_ctl |= 0x08
        if auto_sleep:
            power_ctl |= 0x10
        if link:
            power_ctl |= 0x20

        self.set_register(ADXL345.REG_POWER_CTL, power_ctl)

    def _send_data_format(self, self_test=0, spi=0, int_invert=0, justify=0):
        data_format = self._range & 0x03

        if justify:
            data_format |= 0x04
        if self._full_resolution:
            data_format |= 0x08
        if int_invert:
            data_format |= 0x20
        if spi:
            data_format |= 0x40
        if self_test:
            data_format |= 0x80

        self.set_register(ADXL345.REG_DATA_FORMAT, data_format)

    def _set_fifo_mode(self, mode=0, trigger=0, samples=0x1F):
        fifo_ctl = samples & 0x1F
        fifo_ctl = fifo_ctl | ((mode & 0x03) << 6)

        if trigger:
            fifo_ctl |= 0x20

        self.set_register(ADXL345.REG_FIFO_CTL, fifo_ctl)

    def power_on(self):
        self._set_power_ctl(True)

    def power_off(self):
        self._set_power_ctl(False)

    def set_range(self, _range, full_resolution=True):
        """
        Set the G range and the resolution. Valid range values are 2, 4, 8, 16.
        Full resolution set either 10-bit or 13-bit resolution
        """
        if _range == 2:
            range_code = 0x0
        elif _range == 4:
            range_code = 0x1
        elif _range == 8:
            range_code = 0x2
        elif _range == 16:
            range_code = 0x3
        else:
            raise ValueError("invalid range [" + str(_range) + "] expected one of [2, 4, 8, 16]")

        self._range = range_code
        self._full_resolution = full_resolution
        self._send_data_format()

    def get_axes(self, unit='g'):
        """ return values for the 3 axes of the ADXL, expressed in g (multiple of earth gravity) """
        _bytes = self.get_registers(ADXL345.REG_DATAX0, 6)
        x = self._convert(_bytes[0], _bytes[1], unit=unit)
        y = self._convert(_bytes[2], _bytes[3], unit=unit)
        z = self._convert(_bytes[4], _bytes[5], unit=unit)
        return {'x': x,
                'y': y,
                'z': z}

    def get_fifo_count(self):
        count = self.get_register(ADXL345.REG_FIFO_STATUS)
        return count & 0x7F

    def get_fifo(self):
        """ return an array of the whole FIFO """
        fifo_count = self.get_fifo_count()
        fifo = []
        for num in range(0, fifo_count):
            fifo.append(self.get_axes())
        return fifo

    def enable_fifo(self, stream=True):
        if stream:
            self._set_fifo_mode(mode=0x02)
        else:
            self._set_fifo_mode(mode=0x01)

    def disable_fifo(self):
        self._set_fifo_mode(mode=0x00)

    def set_offset(self, x, y, z):
        """ set hardware offset for the 3 axes of the ADXL, units are g """

        def convert_offset(value):
            value = value / ADXL345.SCALE_FACTOR / 4
            _bytes = int(value) & 0xFF
            return _bytes

        self.set_register(ADXL345.REG_OFSX, convert_offset(x))
        self.set_register(ADXL345.REG_OFSY, convert_offset(y))
        self.set_register(ADXL345.REG_OFSZ, convert_offset(z))

    def calibrate(self):
        """
        Auto calibrate the device offset. Put the device so as one axe is parallel to the gravity field
        (usually, put the device on a flat surface)
        """
        self.set_offset(0, 0, 0)
        samples = self.get_axes()

        x = samples['x']
        y = samples['y']
        z = samples['z']

        abs_x = math.fabs(x)
        abs_y = math.fabs(y)
        abs_z = math.fabs(z)

        # Find which axe is in the field of gravity and set its expected value to 1g absolute value
        if self._equal(abs_x, 1) and self._equal(abs_y, 0) and self._equal(abs_z, 0):
            cal_x = 1 if x > 0 else -1
            cal_y = 0
            cal_z = 0
        elif self._equal(abs_x, 0) and self._equal(abs_y, 1) and self._equal(abs_z, 0):
            cal_x = 0
            cal_y = 1 if y > 0 else -1
            cal_z = 0
        elif self._equal(abs_x, 0) and self._equal(abs_y, 0) and self._equal(abs_z, 1):
            cal_x = 0
            cal_y = 0
            cal_z = 1 if z > 0 else -1
        else:
            raise ValueError("Could not determine ADXL position. One axe should be set in field of gravity")

        offset_x = cal_x - x
        offset_y = cal_y - y
        offset_z = cal_z - z

        self.set_offset(offset_x, offset_y, offset_z)

        return {'x': offset_x,
                'y': offset_y,
                'z': offset_z}

    def close(self):
        self.spi.close()


if __name__ == '__main__':
    adxl = ADXL345()

    print('Device found, ID: {}'.format(hex(adxl.get_device_id())))

    adxl.power_on()

    # adxl.calibrate()

    import time

    while True:
        print(adxl.get_axes())
        time.sleep(.5)
